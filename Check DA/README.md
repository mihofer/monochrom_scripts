Small example on how to track for 4D DA.
First, install python packages using `pip install -r python-requirements.txt`.
Then run `python check_da.py`. The initialized coordinates and outcoordinates together with the survival state will be saved in a csv file.