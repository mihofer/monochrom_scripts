import json
from pathlib import Path
import numpy as np

import xtrack as xt
import xpart as xp
import xobjects as xo

import xdyna as xd

line = xt.Line.from_json(
    'ring_monochrom_ip1246_thin.json')

ref_particle = xt.Particles(mass0=xt.ELECTRON_MASS_EV,
                                 p0c=62.5*10**9) 

line.particle_ref = ref_particle
line.build_tracker()
# since RF is not properly set up, dont use long. coordinates in tracking
line.freeze_longitudinal()
line.twiss_default['method'] = '4d'

# specify the number of turns
nturns = 1000
# create DA element, and init with proper normalized emittances
DA = xd.DA(name=f'fcc_ee_mono',
            normalised_emittance=[
    0.71e-9*ref_particle.beta0[0]*ref_particle.gamma0[0],
    1.42E-12*ref_particle.beta0[0]*ref_particle.gamma0[0]
    ],
            max_turns=nturns,
            use_files=False)
# generate the initial grid to be tracked
DA.generate_initial_grid(x_min=-40, x_max=40, x_step=2,y_min=0, y_max=60, y_step=4, delta=0.000)
DA.line = line
# start tracking
DA.track_job()

# translate DA results to pandas and save as csv
da_df = DA.to_pandas(full=True)
da_df.to_csv('da.csv')
