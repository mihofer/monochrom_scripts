SET, FORMAT="19.15f";
option,update_from_parent=true; // new option in mad-x as of 2/2019


!--------------------------------------------------------------
! Lattice selection and beam parameters
!--------------------------------------------------------------

CALL, FILE="../Lattice/fccee_z.seq";

pbeam :=   45.6;
EXbeam = 7.1e-10;
EYbeam = 1.42e-12;
Nbun :=    10000;
NPar :=   243000000000.0;
HalfCrossingAngle = 0.015;
Ebeam := sqrt( pbeam^2 + emass^2 );

BEAM, PARTICLE=ELECTRON, NPART=Npar, KBUNCH=Nbun, ENERGY=Ebeam, RADIATE=FALSE, BV=+1, EX=EXbeam, EY=EYbeam;

!-------------------------------------------------------------------------------
! Perform initial TWISS and survey in an ideal machine without radiation
!-------------------------------------------------------------------------------

USE, SEQUENCE = FCCEE_P_RING;

// Save the voltage settings for the cavities for later use if needed
VOLTCA1SAVE = VOLTCA1; 
VOLTCA2SAVE = VOLTCA2; 

SHOW, VOLTCA1SAVE, VOLTCA2SAVE;

// Turn off the cavities for ideal machine twiss and survey
VOLTCA1 = 0;
VOLTCA2 = 0;


// Place where the initial conditions are saved - used for RF matching later if needed
SAVEBETA, LABEL=B.IP, PLACE=#s, SEQUENCE=FCCEE_P_RING;

TWISS, FILE = "twiss_z_b1_nottapered.tfs"; ! Twiss without radiation and tapering

!-------------------------------------------------------------------------------
! Perform RF matching and tapering if radiation is on
!-------------------------------------------------------------------------------

  // RF back on
  VOLTCA1 = VOLTCA1SAVE;
  VOLTCA2 = VOLTCA2SAVE;

  // Turn the beam radiation on. N.B. This simple toggle works only if the sequence is not defined in the original beam command.
  BEAM, RADIATE=TRUE;

  // RF matching

  MATCH, sequence=FCCEE_P_RING, BETA0 = B.IP, tapering;
    VARY, NAME=LAGCA1, step=1.0E-7;
    VARY, NAME=LAGCA2, step=1.0E-7;
    CONSTRAINT, SEQUENCE=FCCEE_P_RING, RANGE=#e, PT=0.0;
    JACOBIAN,TOLERANCE=1.0E-14, CALLS=3000;
  ENDMATCH;

  // Twiss with tapering
  USE, SEQUENCE = FCCEE_P_RING;


TWISS, TAPERING, file="twiss_z_b1_tapered.tfs";


! check emittance
EMIT, DELTAP=0.0;
WRITE, TABLE=emit, FILE="emittab_z.tfs";
WRITE, TABLE=emitsumm, FILE="emitsums_z.tfs";