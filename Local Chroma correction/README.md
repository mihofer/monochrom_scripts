Load baseline lattice, and extract sequence from IP to crabsextupole.
Then check chromaticity for nominal setting, for crab sextupole turned off, and then for both sextupoles off.
Last, the vertical chromaticity of the sequence is matched to 0 using the SY1.

To properly set SY2, its strength should be K2SY2 = K2SY1 - crab_factor * crab strength;
Crab strength is given by 1/(L_SY2*THETA_CROSS*BYIP* BY_CS)*sqrt(BXIP/BX_CS); see https://journals.aps.org/prab/pdf/10.1103/PhysRevAccelBeams.19.111005 Eq. B8
The crab factor is determined from Beam-beam studies, at Z it's 97%, W 87%, so ~90% for Higgs mode seems a good starting guess.

To match the Local chroma correction sextupole and crab sextupole proper, the following constraints should be included in the matching.

CONSTRAINT, RANGE=IP/SY1, MUX= 0.5, MUY = 0.75;
CONSTRAINT, RANGE=SY1/SY3, MUX=0.5, MUY =0.5;
