SET, FORMAT="19.15f";
option,update_from_parent=true; // new option in mad-x as of 2/2019


!--------------------------------------------------------------
! Lattice selection and beam parameters
!--------------------------------------------------------------

CALL, FILE="../Lattice/fccee_z.seq";
CALL, FILE="./sextupole_knobs.madx";

pbeam :=   45.6;
EXbeam = 7.1e-10;
EYbeam = 1.42e-12;
Nbun :=    10000;
NPar :=   243000000000.0;
HalfCrossingAngle = 0.015;
Ebeam := sqrt( pbeam^2 + emass^2 );

BEAM, PARTICLE=ELECTRON, NPART=Npar, KBUNCH=Nbun, ENERGY=Ebeam, RADIATE=FALSE, BV=+1, EX=EXbeam, EY=EYbeam;

!-------------------------------------------------------------------------------
! Perform initial TWISS and survey in an ideal machine without radiation
!-------------------------------------------------------------------------------

USE, SEQUENCE = FCCEE_P_RING;

// Save the voltage settings for the cavities for later use if needed
VOLTCA1SAVE = VOLTCA1; 
VOLTCA2SAVE = VOLTCA2; 

SHOW, VOLTCA1SAVE, VOLTCA2SAVE;

// Turn off the cavities for ideal machine twiss and survey
VOLTCA1 = 0;
VOLTCA2 = 0;


// Place where the initial conditions are saved - used for RF matching later if needed
SAVEBETA, LABEL=B.IP, PLACE=#s, SEQUENCE=FCCEE_P_RING;

TWISS; ! Twiss without radiation and tapering

MATCH, SEQUENCE=FCCEE_P_RING;
VARY, NAME=KN_SF;
VARY, NAME=KN_SD;

GLOBAL, DQ1=5; ! specify horizontal chromaticity target here
GLOBAL, DQ2=5; ! specify vertical chromaticity target here

LMDIF, CALLS=10000;
ENDMATCH;


TWISS;