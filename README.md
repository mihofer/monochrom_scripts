# Scripts for the integration of the monochromatization optics in the FCC-ee

Assortment of scripts for creating a lattice for monochromatization.

    - `Emittance check` check the emittance in the FCC-ee with radiation and tapering
    - `Integration` integrate new Exp straight in baseline lattice
    - `Lattice` contains baseline and monochrom sequence
    - `Local Chroma correction` illustrates correction of the vertical chromaticity from the final doublet
    - `Survey check` generates survey for both baseline and monochromatization sequences to check
    - `Check line and ring` test the sensivity of the FCC-ee lattice to different initial conditions
    - `Global Chroma Matching` illustrates correction of the ring chromaticity using all arc sextupoles
    - `Tune Matching` illustrates correction of the ring tunes using the RF insertion
    - `Check DA` shows how to perform DA tracking using Xsuite/Xdyna
